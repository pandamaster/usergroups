User Groups
=========

Creates the user and groups responsible for running and management of the log migration script

/---/

The log monitoring script will execute this role to create the user,groups and the log directory.

/***************/

Requirements
------------

* Linux
* Ubuntu 
* Apache

Role Variables
--------------

<table>
<thead>
<tr>
<th>Name</th>
<th>Description</th>
<th>Example</th>
</tr>
</thead>
<tbody>
<tr>
<td>log_directory</td>
<td>Directory which the log migration script will migrate the log files.</td>
<td>/var/www/logs/</td>
</tr>
<tr>
<td><code>group</code></td>
<td>Linux Group which the user group and the application writing the log will be included.</td>
<td>sysops-group</td>
</tr>
<tr>
<td><code>log_migration_user</code></td>
<td>The user which the log migration script will run</td>
<td>log-migrator</td>
</tr>
<tr>
<td><code>in_house_application_user</code></td>
<td>The user of the in house application</td>
<td>hub</td>
</tr>

</tbody></table>
  
  

Dependencies
------------

This role has no dependencies.


Example Playbook
----------------

    - hosts: localhost
      roles:
         - usergroups



